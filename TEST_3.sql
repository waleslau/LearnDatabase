-- 1.查询全体学生的姓名、学号、所在系；
SELECT
  s.stuname,
  s.stuid,
  d.dname
FROM
  student s,
  department d
WHERE
  s.did = d.did;
-- 2.查询“计算机学院”有多少学生；
SELECT
  count(s.stuid)
FROM
  student s,
  department d
WHERE
  s.did = d.did
  AND d.dname = '电院';
-- 3.查询计算机科学系全体学生的名单；
SELECT
  s.stuname
FROM
  student s,
  department d
WHERE
  s.did = d.did
  AND d.dname = '电院';
-- 4.不使用聚合函数(Max)，取得最高薪水(给出两种解决方案)。
-- (1) select salary from employee order by salary desc limit 0,1;
-- (2) select salary from employee order by salary WHERE salary >= all (select salary from employee) ;
SELECT
  *
FROM
  student
ORDER BY
  stuid DESC
LIMIT
  0, 1;
-- 5.查询全体学生的姓名、出生年份和所在系，要求用小写字母表示系名；
SELECT
  s.stuname,
  s.birth,
  d.dname
FROM
  student s,
  department d
WHERE
  s.did = d.did;
-- 6.查询选修了课程的学生学号（消除取值重复的行）；
-- (这个是错误的尝试) select distinct s.stuid from student s right join grade g on g.stuid=s.stuid;
SELECT
  DISTINCT stuid
FROM
  grade;
-- 7.查询考试不及格的学生的学号；
SELECT
  DISTINCT stuid
FROM
  grade
WHERE
  grade < 60;
-- 8.查询所有年龄在20以下的学生姓名及其年龄；
-- select s.stunamefrom student s where s.birth > '1998-01-01';
-- 通过“2000-06-05”这样格式的生日来计算年龄的两种方法
-- (1) SELECT * , YEAR(CURDATE()) - YEAR(s.birth) AS age from student s;
-- (2) SELECT * , TIMESTAMPDIFF(YEAR,s.birth,NOW()) as age FROM student s;
SELECT
  s.stuname,
  s.age
FROM
  student s
WHERE
  s.age < 20;
-- 9.查询年龄在20~23岁（包括20岁和23岁）之间的学生的姓名、系别和年龄；
-- select s.stuname d.dname from student s,department d where s.did=d.did and s.birth between '1997-12-31' and '2000-12-31';
SELECT
  s.stuname,
  s.age,
  d.dname
FROM
  student s,
  department d
WHERE
  s.did = d.did
  AND s.age BETWEEN 29 AND 24;
-- 10.查询以“DB_”开头，且倒数第三字符为i的课程的详细情况；
SELECT
  * fron course
WHERE
  course.name LIKE 'DB_%i__';
-- 11.查询没有选修1号课程的学生的姓名。
-- select s.stuname from student s,grade g where s.stuid=g.stuid and g.cid not like '%1';
-- 12.列出教师“张琳”所授“C语言程序设计”课程的成绩单，按照成绩降序排列显示；
SELECT
  g.grade
FROM
  grade g,
  teacher t,
  course c
WHERE
  g.cid = c.cid
  AND c.tid = t.tid
  AND t.tname = '张琳'
  AND c.cname = 'C语言程序设计';
-- 13.列出“于欣”老师所教授的所有课程的信息；
SELECT
  c.*
FROM
  course c,
  teacher t
WHERE
  c.tid = t.tid
  AND t.tname = '于欣';
-- 14.查询“崔涛华”同学已经获得的学分信息；
SELECT
  g.grade
FROM
  grade g,
  student s
WHERE
  g.stuid = s.stuid
  AND s.sname = '崔涛华';
-- 15.计算“苗薇薇”老师讲授的“软件测试”的平均分；
SELECT
  avg(g.grade)
FROM
  grade g,
  teacher t,
  course c
WHERE
  g.tid = t.tid
  AND g.cid = c.cid
  AND t.tname = '崔涛华'
  AND c.cname = '软件测试';
-- 16.用连接查询完成要求：查询和刘晨在同一个系学习的学生的学号、姓名和所在系；
SELECT
  s.stuid,
  s.stuname,
  d.dname
FROM
  student s
  LEFT JOIN department d ON d.did = s.did
WHERE
  s.stuid = (
    SELECT
      stuid
    FROM
      student
    WHERE
      stuname = '刘晨'
  );
-- 17.分别用连接查询和嵌套查询完成要求：查询选修了课程名为“信息系统”的学生学号和姓名。
SELECT
  s.stuid,
  s.stuname
FROM
  student s
  RIGHT JOIN grade g ON s.stuid = g.stuid
WHERE
  g.cid = (
    SELECT
      cid
    FROM
      course
    WHERE
      cname = '信息系统'
  );
-- 18.查询选修2号课程且考试及格的所有学生的学号和姓名；
SELECT
  s.stuid,
  s.stuname
FROM
  student s,
  grade g
WHERE
  s.stuid = g.stuid
  AND g.cid = 'C002'
  AND g.grade > 60;
-- 19.统计每一门课的选课人数，包括课程号及选课人数（查询结果包括没有学生选的课程）；
SELECT
  c.cname
FROM
  course c OUTER
  JOIN grade g ON c.cid = g.cid;
-- 20.查询“数学学院”所有的教授信息；
SELECT
  t.*
FROM
  teacher t,
  department d
WHERE
  t.did = d.did
  AND d.dname = '数学学院'