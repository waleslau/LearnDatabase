-- 创建视图
-- （1）建立信息系学生的视图，视图名为：is_student
CREATE view is_student AS
SELECT
  *
FROM
  student
WHERE
  Sdept = 'IS';
-- （2）建立信息系选修了1号课程的学生视图，视图命名为IS_S1。
CREATE view IS_S1 AS
SELECT
  student.*
FROM
  student,
  sc
WHERE
  student.sno = sc.sno
  AND Sdept = 'IS'
  AND Cno = 1;
-- （3）建立信息系选修了1号课程且成绩在90分以上的学生的视图,视图命名为IS_S2
CREATE view IS_S2 AS
SELECT
  student.*
FROM
  student,
  sc
WHERE
  student.sno = sc.sno
  AND Sdept = 'IS'
  AND Cno = 1
  AND Grade > 90;
-- （4）定义一个反映学生出生年份的视图，视图命名为BT_S
CREATE view BT_S AS
SELECT
  Sno,
  Sname,
  Sage,
  2022 - Sage 出生年份
FROM
  student;
-- （5）将学生的学号及他的平均成绩定义为一个视图，视图命名为S_G。
CREATE view S_G AS
SELECT
  sno,
  avg(grade) 平均成绩
FROM
  sc
GROUP BY
  sno;
-- (6) 将Student表中所有女生记录定义为一个视图,命名为F_Student
CREATE view F_Student AS
SELECT
  *
FROM
  student
WHERE
  ssex = '女';

-- 查询视图
-- （1）在信息系学生的视图中找出年龄小于20岁的学生。。
SELECT
  *
FROM
  is_student
WHERE
  sage < 20;
-- （2）查询选修了1号课程的信息系学生。
SELECT
  *
from
  IS_S1;
-- （3）在S_G视图中查询平均成绩在90分以上的学生学号和平均成绩。
SELECT
  *
from
  S_G
WHERE
  平 均 成 绩 > 80;
-- （4）将信息系学生视图IS_Student中学号200215122的学生姓名改为“刘辰”。
update
  is_student
set
  Sname = '刘辰'
where
  Sno = 200215122;
-- （5）向信息系学生视图IS_S中插入一个新的学生记录：200215129，赵新，20岁
INSERT INTO
  student
VALUES('20215129', '赵新', '男', 20, 'IS');
-- （6）删除信息系学生视图IS_Student中学号为200215129的记录
delete from
  student
where
  sno = 200215129;
-- 用SQL命令删除前面所建立的所有视图
DROP view BT_S;
DROP view IS_S1;
DROP view IS_S2;
DROP view S_G;
DROP view is_student;
DROP view F_Student;
