show tables;
desc course;
desc student;
desc sc;

select * from student;
select * from course;
select * from sc;

select Sname,'Year of Birth:',2014-Sage,lower(Sdept) from student;

select Sname NAME,'Year of Birth:' BIRTH,2014-Sage BIRTHDAY,lower(Sdept) DEPARTMENT from student;

select distinct sno from sc;

select sname from student where sdept='CS';

select sname from student where sage<20;

select distinct sno from sc where grade<60;

select distinct sno from sc where grade>90;

select sname,sdept,sage from student where sage between 20 and 23;

select sname,sdept,sage from student where sage not between 20 and 23;

select sname,ssex from student where sdept in ('CS','MA','IS');

select sname,ssex from student where sdept not in ('CS','MA','IS');

select * from student where sno like '201215121';

select * from student where sno='201215121';

select * from student where sname like '刘%';


select * from student where sname like '欧阳%';

select * from student where sname like '_阳%';

select * from student where sname not like '刘%';

select sno,cno from sc where grade is not null

select sno,cno from sc where grade is null

select sname from student where sdept='CS' and sage<20;

select sname from student where sdept='CS' or sdept='MA' or sdept='IS';

select sno,grade from sc where cno='3' order by grade desc;

select * from student order by sdept,sage desc;

select count(*) from student;

select count(distinct sno) from sc;

select avg(grade) from sc where cno='1';

select max(grade) from sc where cno='1';

select sum(ccredit) from sc,course where sno='201215012' and sc.sno=course.cno;

select cno,count(sno) from sc group by cno;

select count(*) from student;

select sno from sc group by sno having count(*)>3;

select student.*,sc.* from student,sc where student.sno=sc.sno;

select student.sno,sname,ssex,sage,sdept,cno,grade from student,sc where student.sno=sc.sno;

select student.sno,sname,ssex,sage,sdept,cno,grade from student left outer join sc on (student.sno=sc.sno);

select * from sc where grade > 70 and grade <= 90;

-- 视图

-- create view CS_student as select * from student where Sdept='CS';
-- create view IS_student as select * from student where Sdept='IS' with check option;
-- CREATE view BT_S(Sno, Sname, Sbirth) AS SELECT Sno, Sname, 2021 - Sage FROM student;

-- CREATE view S_G(Sno,Gvag) as SELECT Sno,AVG(Grade) FROM sc group by Sno;

-- create view IS_student2 as select * from IS_student where Sdept='IS';
-- 删除
-- drop view IS_student
-- 级联删除，好像不太好使？
-- drop view IS_student cascade


