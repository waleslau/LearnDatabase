/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : 192.168.190.6:3306
 Source Schema         : studentsdb

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 09/04/2022 10:50:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `cid` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `cname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NULL DEFAULT NULL,
  `score` int NULL DEFAULT NULL,
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('C001', '计算机应用基础', 2);
INSERT INTO `course` VALUES ('C002', 'C语言程序设计', 2);
INSERT INTO `course` VALUES ('C003', '数据库原理及应用', 2);
INSERT INTO `course` VALUES ('C004', '英语', 4);
INSERT INTO `course` VALUES ('C005', '高等数学', 4);
INSERT INTO `course` VALUES ('C006', '软件工程', 4);
INSERT INTO `course` VALUES ('C007', '数据结构', 4);
INSERT INTO `course` VALUES ('C008', '组成原理', 4);

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `did` char(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL COMMENT '院系编号',
  `dname` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL COMMENT '院系名称',
  PRIMARY KEY (`did`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('d001', '电院');
INSERT INTO `department` VALUES ('d002', '外语学院');
INSERT INTO `department` VALUES ('d003', '体育学院');
INSERT INTO `department` VALUES ('d004', '商学院');
INSERT INTO `department` VALUES ('d005', '马院');
INSERT INTO `department` VALUES ('d006', '堪院');

-- ----------------------------
-- Table structure for grade
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade`  (
  `stuid` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `cid` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `grade` int NULL DEFAULT NULL,
  PRIMARY KEY (`stuid`, `cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of grade
-- ----------------------------
INSERT INTO `grade` VALUES ('0001', 'C001', 80);
INSERT INTO `grade` VALUES ('0001', 'C002', 91);
INSERT INTO `grade` VALUES ('0001', 'C003', 88);
INSERT INTO `grade` VALUES ('0001', 'C004', 85);
INSERT INTO `grade` VALUES ('0001', 'C005', 77);
INSERT INTO `grade` VALUES ('0002', 'C001', 77);
INSERT INTO `grade` VALUES ('0002', 'C002', 68);
INSERT INTO `grade` VALUES ('0002', 'C003', 80);
INSERT INTO `grade` VALUES ('0002', 'C004', 79);
INSERT INTO `grade` VALUES ('0002', 'C005', 73);
INSERT INTO `grade` VALUES ('0003', 'C001', 84);
INSERT INTO `grade` VALUES ('0003', 'C002', 92);
INSERT INTO `grade` VALUES ('0003', 'C003', 81);
INSERT INTO `grade` VALUES ('0003', 'C004', 82);
INSERT INTO `grade` VALUES ('0003', 'C005', 75);
INSERT INTO `grade` VALUES ('0004', 'C001', 74);
INSERT INTO `grade` VALUES ('0004', 'C002', 42);
INSERT INTO `grade` VALUES ('0004', 'C003', 91);
INSERT INTO `grade` VALUES ('0004', 'C004', 62);
INSERT INTO `grade` VALUES ('0004', 'C005', 50);
INSERT INTO `grade` VALUES ('0005', 'C001', 64);
INSERT INTO `grade` VALUES ('0005', 'C002', 72);
INSERT INTO `grade` VALUES ('0005', 'C003', 81);
INSERT INTO `grade` VALUES ('0005', 'C004', 52);
INSERT INTO `grade` VALUES ('0005', 'C005', 89);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `stuid` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `stuname` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `sex` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NULL DEFAULT NULL,
  `Did` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `birth` date NULL DEFAULT NULL,
  `addr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NULL DEFAULT NULL,
  PRIMARY KEY (`stuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('0001', '张青平', '男', 'd001', '2000-10-01', '衡阳市东风路77号');
INSERT INTO `student` VALUES ('0002', '刘东阳', '男', 'd002', '1998-12-09', '东阳市八一北路33号');
INSERT INTO `student` VALUES ('0003', '马晓夏', '女', 'd003', '1995-05-12', '长岭市五一路763号');
INSERT INTO `student` VALUES ('0004', '钱忠理', '男', 'd004', '1994-09-23', '滨海市洞庭大道279号');
INSERT INTO `student` VALUES ('0005', '孙海洋', '男', 'd005', '1995-04-03', '长岛市解放路27号');
INSERT INTO `student` VALUES ('0006', '郭小斌', '男', 'd006', '1997-11-10', '南山市红旗路113号');
INSERT INTO `student` VALUES ('0007', '肖月玲', '女', 'd001', '1996-12-07', '东方市南京路11号');
INSERT INTO `student` VALUES ('0008', '张玲珑', '女', 'd002', '1997-12-24', '滨江市新建路97号');
INSERT INTO `student` VALUES ('0009', '王玲', '女', 'd003', '1998-12-24', '郑州市新建路97号');
INSERT INTO `student` VALUES ('0010', '王华', '男', 'd004', '1997-01-24', '南阳市建设路97号');
INSERT INTO `student` VALUES ('0011', '王小丽', '女', 'd005', '1996-10-24', '滨海市秦南路47号');
INSERT INTO `student` VALUES ('0012', '李小佳', '女', 'd006', '1995-10-04', ' ');
INSERT INTO `student` VALUES ('0013', '李宁', '男', 'd006', '1993-01-04', ' ');
INSERT INTO `student` VALUES ('0014', '景天', '男', 'd003', '1997-11-04', NULL);

-- ----------------------------
-- Table structure for teach
-- ----------------------------
DROP TABLE IF EXISTS `teach`;
CREATE TABLE `teach`  (
  `Tid` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL,
  `Cid` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teach
-- ----------------------------
INSERT INTO `teach` VALUES ('88771', 'c001');
INSERT INTO `teach` VALUES ('86811 ', 'c002');
INSERT INTO `teach` VALUES ('78896 ', 'c003');
INSERT INTO `teach` VALUES ('74906 ', 'c004');
INSERT INTO `teach` VALUES ('66997 ', 'c005');
INSERT INTO `teach` VALUES ('53021 ', 'c006');
INSERT INTO `teach` VALUES ('43703 ', 'c007');
INSERT INTO `teach` VALUES ('36762', 'c005');
INSERT INTO `teach` VALUES ('25306', 'c004');
INSERT INTO `teach` VALUES ('23364', 'c007');
INSERT INTO `teach` VALUES ('23364', 'c008');
INSERT INTO `teach` VALUES ('25306', 'c009');
INSERT INTO `teach` VALUES ('66997', 'c001');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `Tid` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL COMMENT '教师编号',
  `TNAME` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL COMMENT '教师姓名',
  `Did` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_zh_0900_as_cs NOT NULL COMMENT '院系编号',
  PRIMARY KEY (`Tid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_zh_0900_as_cs ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('23364', '杜倩颖', 'd001');
INSERT INTO `teacher` VALUES ('25306', '赵复前', 'd002');
INSERT INTO `teacher` VALUES ('36762', '胡祥', 'd003');
INSERT INTO `teacher` VALUES ('43703', '何纯', 'd004');
INSERT INTO `teacher` VALUES ('53021', '刘小娟', 'd005');
INSERT INTO `teacher` VALUES ('66997', '章梓雷', 'd006');
INSERT INTO `teacher` VALUES ('74906', '胡建君', 'd001');
INSERT INTO `teacher` VALUES ('78896', '黄阳', 'd002');
INSERT INTO `teacher` VALUES ('86811', '胡冬', 'd003');
INSERT INTO `teacher` VALUES ('88771', '许杰', 'd004');

SET FOREIGN_KEY_CHECKS = 1;
