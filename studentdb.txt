select * from student where birth between '1997-1-1' and '1997-12-31' or birth between '1998-1-1' and '1998-12-31';

select * from student where stuname like '%小%';

select * from student where stuname like '张%';

select stuid,stuname,addr from student where addr like '%滨海%';

select * from student where stuname not like '张%';

select * from student where stuname like '__';

select * from student where stuname like '王_';

select * from student where addr is NULL;

select * from student where addr is not NULL;

select * from grade order by grade desc,stuid asc;
 
select * from grade order by grade desc,stuid asc limit 10;

-- 从第0条开始，查5条
select * from grade order by grade desc,stuid asc limit 0,5;

select * from grade order by grade desc,stuid asc limit 5,5;

select * from student order by birth desc limit 5;

select * from student order by birth desc limit 5;

select stuid,avg(grade) from grade group by stuid;

select stuid as 学号,avg(grade) as 平均分 from grade group by stuid;

select cid as 课程号,avg(grade) as 平均分 from grade group by cid;

-- ？？？？
select stuid as 学号,cid as 课程号,sum(grade) as 总分 from grade group by stuid,cid;

select sex as 性别, count(*) as 总数 from student group by sex;

-- 使用roolup汇总能参与运算的数据
select sex as 性别, count(*) as 总数 from student group by sex with rollup;

-- 交叉连接，相当于笛卡尔积
select t.tname,s.stuname from student s cross join teacher t;
select c.cname,s.stuname from student s cross join course c;

select distinct c.cname, c.cid from course c,grade g where c.cid=g.cid;

select * from student s,department d where s.did=d.did and d.dname='电院';

select s.stuname 学生姓名,g.grade 分数 from student s,department d,grade g where s.did=d.did and s.stuid=g.stuid and d.dname='电院';

select s.stuname 学生姓名,c.cname 课程名, g.grade 分数 from student s,department d,grade g, course c where s.did=d.did and s.stuid=g.stuid and c.cid=g.cid and d.dname='电院';


select distinct t.tname,s.stuname from teacher t,teach,course c,grade g,student s where t.tid=teach.tid and g.cid=c.cid and g.stuid=s.stuid and t.tname='章梓雷';

select distinct t.tname,s.stuname from teacher t,teach,course c,grade g,student s where t.tid=teach.tid and g.cid=c.cid and g.stuid=s.stuid and t.tname='章梓雷';

select * from teacher,teach where teacher.tid=teach.tid;

select * from teacher,teach,course where teacher.tid=teach.tid and teach.cid=course.cid;

select * from teacher,teach,course where teacher.tid=teach.tid and teach.cid=course.cid and course.cname='软件工程';

select * from teacher,teach,course where teacher.tid=teach.tid and teach.cid=course.cid and cname='软件工程';

select
  course.cid,
  course.cname,
  grade.grade
from
  student
  inner join grade on student.stuid = grade.stuid -- 前两个表必须有交集
  inner join course on course.cid = grade.cid;


SELECT
  *
FROM
  student
  INNER JOIN grade ON student.stuid = grade.stuid
  INNER JOIN department ON student.Did = department.did
WHERE
  department.dname = '电院'
  AND grade.grade > 70;



SELECT
  *
FROM
  student
  -- INNER JOIN grade ON student.stuid = grade.stuid
  -- INNER JOIN department ON student.Did = department.did
  -- 使用 using 简化连接命令
  INNER JOIN grade USING(stuid)
  INNER JOIN department USING(did)
WHERE
  department.dname = '电院'
  AND grade.grade > 70;


SELECT
  DISTINCT student.stuid,
  student.stuname,
  grade.cid,
  grade.grade
FROM
  student -- student 是左表
  -- LEFT OUTER JOIN grade USING(stuid) -- 做左外链接（右表向左表看齐）;
  RIGHT OUTER JOIN grade USING(stuid) -- 做右外链接（左表向右表看齐）;


-- 嵌套查询
SELECT
  dname
FROM
  department
WHERE
  did = (
    SELECT
      did
    FROM
      teacher
    WHERE
      tname LIKE '胡__'
  );

SELECT
  stuid,
  stuname,
  sex
from
  student
where
  birth = (
    SELECT
      min(birth)
    from
      student
  );

SELECT
  stuid,
  stuname
FROM
  student
WHERE
  stuid IN (
    SELECT
      stuid
    FROM
      grade
    WHERE
      grade.grade > 80
      AND cid IN (
        SELECT
          cid
        FROM
          course
        WHERE
          score > 2
      )
  )
ORDER BY
  stuid DESC;


SELECT
  stuid,
  stuname,
  sex
FROM
  student
WHERE
  birth > some -- any/some 任意/某些 | all 全部
  (
    SELECT
      birth
    FROM
      student
    WHERE
      did IN (
        SELECT
          did
        FROM
          department
        WHERE
          dname = '电院'
      )
  )
