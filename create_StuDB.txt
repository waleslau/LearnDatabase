DROP DATABASE IF EXISTS StuDB;
create database StuDB default character set utf8mb4;
use StuDB;

create table student
(
Sno int,
Sname varchar(255),
Ssex varchar(10),
Sage int,
Sdept varchar(255),
constraint s_pk primary key(Sno)
);

INSERT INTO student VALUES('201215121','李勇','男', 20,'CS');

INSERT INTO student VALUES('201215122','张立','男', 19,'IS');

INSERT INTO student VALUES('201215123','刘晨','女', 19,'CS');

INSERT INTO student VALUES('201215125','王敏','女', 18,'MA');


create table course
(
Cno int,
Cname varchar(255),
Cpno int,
Ccredit int,
constraint c_pk primary key(Cno),
constraint s_fk foreign key(cpno) references course(cno)
);

INSERT INTO course VALUES(1,'数据库',5,4);
INSERT INTO course VALUES(2,'数学',NULL,2);
INSERT INTO course VALUES(3,'信息系统',1,4);
INSERT INTO course VALUES(4,'操作系统',6,3);
INSERT INTO course VALUES(5,'数据结构',7,4);
INSERT INTO course VALUES(6,'数据处理',NULL,2);
INSERT INTO course VALUES(7,'PASCAL语言',6,4);

create table sc
(
Sno int,
Cno int,
Grade int,
check(Grade<100),
constraint pk_sc primary key(Sno,Cno),
constraint fk_Sno foreign key(Sno)
references student(Sno),
constraint fk_Cno foreign key(Cno)
references course(Cno)
);

INSERT INTO sc VALUES(201215121,1,92);
INSERT INTO sc VALUES(201215121,2,85);
INSERT INTO sc VALUES(201215121,3,88);
INSERT INTO sc VALUES(201215122,2,90);
INSERT INTO sc VALUES(201215122,3,80);